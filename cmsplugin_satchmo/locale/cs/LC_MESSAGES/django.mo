��            )         �     �     �     �     �       ]     \   z  U   �  b   -  +   �     �     �     �     �                    )     /     A     N     \     c     k     x     �     �     �     �     �  �  �     J      ]      ~     �     �  m   �  j   2  g   �  �   	  /   �	     �	     �	     �	     �	  	   
     
     (
     =
     E
     L
     Y
     e
     v
     ~
     �
     �
     �
     �
     �
     �
         	                                                                                       
                                        Alternative text CMS plugin Satchmo Setting CMS plugin Satchmo Settings Categories listing Category detail E.g. if in your SATCHMO_SETTINGS is defined CATEGORY_SLUG as "category" fill "category" here. E.g. if in your SATCHMO_SETTINGS is defined PRODUCT_SLUG as "products" fill "products" here. E.g. if in your SATCHMO_SETTINGS is defined SHOP_BASE as "/shop/" fill "/shop/" here. Leave blank for listing root categories or select main category for listing their child categries. Name of CSS class for template cusomization Product detail Products listing bestsellers categories list from site category category slug category's path class featured products kind of list main category number product product slug product's path products list from site recent products shop base slug site title Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-18 18:11+0200
PO-Revision-Date: 2014-05-15 17:55+0200
Last-Translator: Tomáš Benda <tomas@labs.cz>
Language-Team: LANGUAGE <LL@li.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Alternativní text Satchmo - nastavení CMS pluginu Satchmo - nastavení CMS pluginu Seznam kategorií Odkaz na kategorii Např. pokud máte v SATCHMO_SETTINGS nadefinováno CATEGORY_SLUG jako "kategorie", vyplňte zde "kategorie". Např. pokud máte v SATCHMO_SETTINGS nadefinováno PRODUCT_SLUG jako "produkty", vyplňte zde "produkty". Např. pokud máte v SATCHMO_SETTINGS nadefinováno SHOP_BASE jako "/obchod/", vyplňte zde "/obchod/". Pokud chcete seznam hlavních kategorií, ponechte prázdné. Vyberete-li nějakou kategorii, dostanete seznam jejích podkategorií. Jméno CSS třídy pro přizpůsobení šablony Odkaz na produkt Seznam produktů nejlépe prodávané kategorie z webu kategorie slug umístění kategorie cesta ke kategoriím třída v akci druh seznamu z kategorie počet produktů produkt slug umístění produktu cesta k produktům produkty z webu nejnovější slug umístění obchodu web popisek 